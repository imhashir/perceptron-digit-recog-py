from mnist import MNIST
import csv
from math import exp
import numpy as np


def sigmoid(value):
    if value < 0:
        return 1 - 1 / (1 + exp(value))
    else:
        return 1 / (1 + exp(-value))


def predict_all(data, ws):
    return [1 if sigmoid(sum([data[i] * float(w[i]) for i in range(len(data))]) + float(w[-1])) > 0 else 0 for w in ws]
    # return [1 if sigmoid(np.dot(data, w[:len(data)]) + int(w[-1])) > 0 else 0 for w in ws]


def predict(data, ws):
    predicted_values = predict_all(data, ws)
    predicted_indices = []
    for i in range(10):
        if predicted_values[i] is 1:
            predicted_indices.append(i)
    return predicted_indices


def train_weights(train_data, outputs, dig_weights):
    sum_error = -1
    iterations = 6
    learning_rate = 0.5
    while sum_error != 0.0 and iterations > 0:
        sum_error = 0.0
        for i in range(len(train_data)):
            predictions = predict_all(train_data[i], dig_weights)

            errors = [(1 if k == outputs[i] else 0) - predictions[k] for k in range(10)]

            sum_error += sum([error**2 for error in errors])

            for k in range(10):
                dig_weights[k][-1] += errors[k]*learning_rate

            for k in range(10):
                for j in range(len(train_data[i])):
                    dig_weights[k][j] += train_data[i][j] * errors[k] * learning_rate

        iterations -= 1
        print(iterations, ". Sum Error: ", sum_error)

    return dig_weights


mn_data = MNIST(r"E:\FAST\Semester 6\AI\Assignments\2\sample")

while True:
    choice = int(input("\n\n1. Train\n"
                       "2. Load already saved Perceptron\n"
                       "3. Test an Image\n"
                       "4. Find Accuracy\n"
                       "5. Exit\n"))

    if choice is 1:
        train_images, train_labels = mn_data.load_training()
        weights = [[0 for i in range(len(train_images[0]) + 1)] for j in range(10)]
        data_size = int(input("Enter train data size: "))
        weights = train_weights(train_images[:data_size], train_labels[:data_size], weights)

        with open("perceptrons.csv", 'w', newline='') as csv_file:
            writer = csv.writer(csv_file, delimiter=',')
            for weight in weights:
                writer.writerow(weight)
    elif choice is 2:
        i = 0
        weights = [[None]*734]*10
        with open("perceptrons.csv", 'r', newline='') as csv_file:
            reader = csv.reader(csv_file, delimiter=',')
            for weight in reader:
                weights[i] = [float(w) for w in weight]
                i += 1
    elif choice is 3:
        i = 0
        test_images, test_labels = mn_data.load_testing()
        print(mn_data.display(test_images[100]))
        print("\nPossible matches: ")
        print(predict(test_images[100], weights))
    elif choice is 4:
        correct = 0.0
        totalSamples = int(input("Enter number data size: "))
        test_images, test_labels = mn_data.load_testing()
        for index in range(totalSamples):
            ret_list = predict(test_images[index], weights)
            for ret_item in ret_list:
                if test_labels[index] == ret_item:
                    correct += 1
        print("Accuracy: ", (correct/totalSamples)*100)
    elif choice is 5:
        break
